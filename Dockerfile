FROM mysql:5.7

# Přidat vlastní konfigurační soubor
COPY assets/*.cnf /etc/mysql/conf.d/
RUN chmod 644 /etc/mysql/conf.d/*.cnf
