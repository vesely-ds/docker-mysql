Docker image pro [MySQL](https://www.mysql.com/)
================================================

Rozdíly proti [oficiální verzi](https://hub.docker.com/_/mysql/):

* České národní prostředí (UTF-8, řazení a české formáty času a data)


Jak používat
------------

Příklad `docker-compose.yml`, který:

* Nastaví root heslo na `pass`.
* Vytvoří databázi a uživatele `demo` s heslem `pass`.
* Perzistentní data ukládá do lokáního adresáře `./data`.
* Vystaví port 3306, na kterém je možné se k DB připojit.


```
#!docker
version: '2'
services:
  db:
    image: vesely/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=pass
      - MYSQL_DATABASE=demo
      - MYSQL_USER=demo
      - MYSQL_PASSWORD=pass
    volumes:
      - ./data:/var/lib/mysql
    ports:
      - 3306:3306
```

          
Vytvoření a spuštění kontejneru:

    docker-compose up